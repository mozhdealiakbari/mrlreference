### Test Scripts

To test the Data Model, please follow the instructions:

1. Ensure Python dependencies are installed.
```
pip3 install requirements.txt
```

2. Downloand [100 test samples](https://facesyntheticspubwedata.blob.core.windows.net/iccv-2021/dataset_100.zip) and unpack to the folder '\tests\dataset_100'
```
curl https://facesyntheticspubwedata.blob.core.windows.net/iccv-2021/dataset_100.zip -O -J -L
unzip dataset_100.zip
```

3. Generate a Python Data Model from the JSON file and load and visualize a sample from the dataset using the test script.
```
./generate_interface.sh
python facesynthetics_test.py
``` 

4. Check the following files were successfully created visualizing the annotations of the dataset which confirms the correctness of the Data Model.
```
000055_vis_heatmap.png
000055_vis_segmentation.png
```
