## MRL Eye Dataset

MRL Eye Dataset, the large-scale dataset of human eye images. This dataset contains infrared images in low and high resolution, all captured in various lightning conditions and by different devices. The dataset is suitable for testing several features or trainable classifiers. In order to simplify the comparison of algorithms, the images are divided into several categories, which also makes them suitable for training and testing classifiers. It was introduced in this [paper](https://link.springer.com/chapter/10.1007/978-3-030-03801-4_38
).

This repository provides an example of dataset description, including a data model, for the MRL Eye Dataset. 
- The PPTX (and CSV) files can be found under [docs/](docs).
- The JSON data model can be found in the [dataset.json](https://gitlab.com/mozhdealiakbari/mrlreference).

### Data Model

The data model is intended to help document datasets before using them with Bonseyes Datatools. The MRL Eye data model is created from a single JSON file that contains a serialization example of the dataset with a single example entry and complete information of annotation categories. This JSON covers the following:

* **gender** [0 - man, 1 - woman]; the dataset contains the information about gender for each image (man, woman)  
* **glasses** [0 - no, 1 - yes]; the information if the eye image contains glasses is also provided for each image (with and without the glasses)
* **reflections** [0 - none, 1 - small, 2 - big]; Three reflection states based on the size of reflections (none, small, and big reflections) have been annotated.
* **lighting conditions** [0 - bad, 1 - good]; each image has two states (bad, good) based on the amount of light during capturing the videos
* **sensor ID** [01 - RealSense, 02 - IDS, 03 - Aptina]; at this moment, the dataset contains the images captured by three different sensors (Intel RealSense RS 300 sensor with 640 x 480 resolution, IDS Imaging sensor with 1280 x 1024 resolution, and Aptina sensor with 752 x 480 resolution)


### Licensing and Contributing

This project is licensed under the terms and conditions of the MIT License. A copy of the license is accessible here: [LICENSE](LICENSE).

Details on contributing to this project are found in the file [docs/CONTRIBUTING.md](docs/CONTRIBUTING.md).

### Disclaimer

Bonseyes Community Association provides only scripts for downloading and preprocessing public datasets. We do not own these datasets and are not responsible for their quality or maintenance.

Please ensure that you have the permission to use a dataset under the dataset's license. We are not a law firm and does not provide legal advice. Our open source tools are meant to be a jumping-off point, not a final word on whether your use is compatible with a particular license. For example, some licenses may allow commercial use, but still have requirements about attribution and other things. It is your responsibility alone to read the license and follow it.

Bonseyes Community Association provides this information and tools on an “as-is” basis without warranties of any kind, and disclaims liability for all damages resulting from your use of the license information and tools. Please seek the advice of a licensed legal professional in your jurisdiction if you have any questions about a particular license.

To dataset owners: If you do not want your dataset to be included on Bonseyes, or wish to update it in any way, we will remove or update all public content upon your request. You can contact us with Gitlab issues. Your understanding and contribution to this community are greatly appreciated.
