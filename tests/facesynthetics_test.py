#!/usr/bin/env python

"""facesynthetics_test.py: Extract and load Face Synthetics dataset using json datamodel."""

import json
import prettyprinter
import ubelt as ub
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.colors import ListedColormap
matplotlib.use("Agg")

from skimage.io import imread
from pathlib import Path
from typing import Tuple

# Visualisation
import cv2
import numpy as np
from scipy.stats import multivariate_normal

import sys
sys.path.append('..')
from facesynthetics_datamodel import FaceSynthetics
from datetime import date, datetime, time

class FaceSyntheticsDataset(object):
    """
    A class used to extract and load Face Synthetics dataset from a single folder from an extracted .zip file containing color images, segmentation images, and 2D landmark coordinates in a text file.

    dataset_{#_of_samples}.zip -> root_dir / samples_dir
    ├── {image_id}.png        # Rendered image of a face at a given resolution
    ├── {image_id}_seg.png    # Segmentation image, where each pixel has an integer value mapping to the segmentation categories
    ├── {image_id}_ldmks.txt  # Landmark annotations for 70 facial landmarks (x, y) coordinates for every row
    
    """
    
    CLASSES = [
        'background',
        'skin',
        'nose',
        'r-eye',
        'l-eye',
        'r-brow',
        'l-brow',
        'r-ear',
        'l-ear',
        'i-mouth',
        't-lip',
        'b-lip',
        'neck',
        'hair',
        'beard',
        'clothing',
        'glasses',
        'headwear',
        'facewear'
    ]
    
    PALETTE = [
        [0, 0, 0],
        [204, 0, 0],
        [76, 153, 0],
        [204, 204, 0],
        [51, 51, 255],
        [204, 0, 204],
        [0, 255, 255],
        [255, 204, 204],
        [102, 51, 0],
        [255, 0, 0], 
        [102, 204, 0],
        [255, 255, 0],
        [0, 0, 153],
        [0, 0, 204],
        [255, 51, 153],
        [0, 204, 204],
        [0, 51, 0],
        [255, 153, 51],
        [0, 204, 0]
    ]
    
    # Details of images of dataset
    IMAGE_WIDTH             = 512
    IMAGE_HEIGHT            = 512
    IMAGE_DEPTH             = 3
    IMAGE_CHANNEL_ORDER     = "rgb"
    
    # Details of sensor used in dataset
    SENSOR_NAME             = "Synthetic"
    SENSOR_MANUFACTURER     = "Blender Foundation"
    SENSOR_PART_NUMBER      = "Blender 2.8x"
    
    # Details of category ids
    CATEGORY_BBOX2D_ID          = 0
    CATEGORY_KEYPOINTS2D_ID     = 0
    CATEGORY_SEGMENTATION_ID    = 0
    
    # Details of metadatum
    METADATUM_BBOX2D_CONFIDENCE         = 1.0
    METADATUM_BBOX2D_CLASSMAP_ID        = 1         # 1 = Face
    METADATUM_KEYPOINTS2D_CONFIDENCE    = 1.0
    METADATUM_SEGMENTATION_CONFIDENCE   = 1.0
    
    def __init__(self, datamodel:str=None, root_dir:str=None, samples_dir:str=None, image_ext:str=None):
    
        super(FaceSyntheticsDataset, self).__init__()
        
        # Dataset source folder and image format used when extract() is called
        self.root_dir                   = root_dir
        self.samples_dir                = samples_dir
        self.image_ext                  = image_ext
        
        # Dataset source images and sensor are extracted in extract()
        self.dataset                        = None
        self.source                         = {}
        self.metadatum                      = {}
        self.category                       = {}
        self.image_list                     = []
        self.sensor_list                    = []
        
        # Dataset annotations and metadatum are extracted in __extract_annotation_metadatum()
        self.annotation_list                = []
        self.metadatum_bbox2d_list          = []
        self.metadatum_keypoints2d_list     = []
        self.metadatum_segmentation_list    = []

        # Dataset categories and samples are extracted from datamodel JSON file in __extract_category() and __extract_sample()
        self.category_bbox2d_list           = []
        self.category_keypoints2d_list       = []
        self.category_segmentation_list     = []
        self.sample_list                    = []
        
        # Load and parse JSON datamodel
        try:
            fid = open(datamodel, 'rb')
        except OSError:
            print(f"Could not open/read dataset JSON datamodel file: {datamodel}")
            sys.exit()
        with fid:
            # Load JSON datamodel
            json_datamodel = json.load(fid)
            
            # Extract categories (including class maps) 
            self.category = self.__extract_category( json_datamodel )
            
            # Extract samples
            self.sample_list = self.__extract_sample( json_datamodel )
            
        return
            

    def __getitem__(self, index):
        image = self.image_list[index]
        annotation = self.annotation_list[index]
        
        # Compute global intrinsics from image size
        (w, h) = (image.width, image.height)
        global_intrinsics = np.array([[w + h, 0, w // 2], [0, w + h, h // 2], [0, 0, 1]])
        
        # Compute bbox intrinsics from bounding box size
        (w, h) = (annotation.bbox2d_annotation[self.CATEGORY_BBOX2D_ID].width, annotation.bbox2d_annotation[self.CATEGORY_BBOX2D_ID].height)
        bbox_intrinsics = np.array([[w + h, 0, w // 2], [0, w + h, h // 2], [0, 0, 1]])

        # Return image, annotation, and global intrinsics
        return image, annotation, global_intrinsics, bbox_intrinsics
    
    def __extract_category(self, json_datamodel:str=None) -> FaceSynthetics.Category:
    
        # Create all Bbox2d class maps and category in json datamodel
        for index, json_bbox2d_category in enumerate(json_datamodel['category']['bbox2d_category']):
            # Create class map
            class_map_bbox2d_list = []
            for class_map_item in json_bbox2d_category['bbox2d_class_map']:
                class_map_bbox2d_list.append(
                    FaceSynthetics.Category.Bbox2dCategory.Bbox2dClassMap(
                        class_map_item['id'],
                        class_map_item['name']
                    )
                )
            # Create category
            self.category_bbox2d_list.append(
                FaceSynthetics.Category.Bbox2dCategory(
                    id_ = index,
                    name = json_bbox2d_category['name'],
                    bbox2d_class_map = class_map_bbox2d_list,
                    human_annotated = json_bbox2d_category['human_annotated']
                )
            )
        
        # Create Keypoints2d class map and category
        for index, json_keypoints2d_category in enumerate(json_datamodel['category']['keypoints2d_category']):
            # Create class map
            class_map_keypoint2d_list = []
            for class_map_item in json_keypoints2d_category['keypoints2d_class_map']:
                class_map_keypoint2d_list.append(
                    FaceSynthetics.Category.Keypoints2dCategory.Keypoints2dClassMap(
                        class_map_item['id'],
                        class_map_item['group_id'],
                        class_map_item['name'],
                        class_map_item['prev_id'],
                        class_map_item['next_id']
                    )
                )
            # Create category
            self.category_keypoints2d_list.append(
                FaceSynthetics.Category.Keypoints2dCategory(
                    id_ = index,
                    name = json_keypoints2d_category['name'],
                    keypoints2d_class_map = class_map_keypoint2d_list,
                    human_annotated = json_keypoints2d_category['human_annotated']
                )
            )
            
        # Create Segmentation Per Pixel class map and category
        for index, json_segmentation_category in enumerate(json_datamodel['category']['segmentation_category']):
            # Create class map
            class_map_segmentation_list = []
            for class_map_item in json_segmentation_category['segmentation_class_map']:
                class_map_segmentation_list.append(
                    FaceSynthetics.Category.SegmentationCategory.SegmentationClassMap(
                        class_map_item['id'],
                        class_map_item['name'],
                        class_map_item['pixel_value'],
                        class_map_item['color_map']
                    )
                )
            # Create category
            self.category_segmentation_list.append(
                FaceSynthetics.Category.SegmentationCategory(
                    id_ = index,
                    name = json_segmentation_category['name'],
                    segmentation_class_map = class_map_segmentation_list,
                    human_annotated = json_segmentation_category['human_annotated']
                )
            )
            
        return FaceSynthetics.Category( self.category_bbox2d_list, self.category_keypoints2d_list, self.category_segmentation_list )    

    def __extract_sample(self, json_datamodel:str=None) -> [FaceSynthetics.Sample]:
    
        for index, sample_item in enumerate(json_datamodel['sample']):
            self.sample_list.append(
                FaceSynthetics.Sample(
                    sample_item['id'],
                    sample_item['name'],
                    sample_item['size_mb'],
                    sample_item['url']
                )
            )
        return self.sample_list

    def __extract_source_sensor(self, sensor_id:int=0) -> [FaceSynthetics.Source.Sensor]:
        sensor = FaceSynthetics.Source.Sensor(
            id_ = sensor_id,
            name = self.SENSOR_NAME,
            manufacturer = self.SENSOR_MANUFACTURER,
            part_number = self.SENSOR_PART_NUMBER,
            properties = None
        )
        return sensor
        
    def __extract_source_image(self, image_id:int=0, image_ref:str=None, sensor_id:int=0) -> [FaceSynthetics.Source.Image]:
        image = FaceSynthetics.Source.Image(
            id_ = image_id,
            image_ref = str(image_ref),
            width = self.IMAGE_WIDTH,
            height = self.IMAGE_HEIGHT,
            depth = self.IMAGE_DEPTH,
            channel_order = str(self.IMAGE_CHANNEL_ORDER),
            sensor_id = sensor_id
        )
        return image
        
    def create_source_sensor(self) -> int:
        sensor = self.__extract_source_sensor()
        # (TODO) - check for duplicates
        self.sensor_list.append(sensor)
        return sensor.id_, sensor
    
    def create_source_image(self, image_id, filename) -> int:
        image = self.__extract_source_image(image_id, filename)
        # (TODO) - check for duplicates
        self.image_list.append(image)
        return image.id_, image
        
    def create_metadatum_bbox2d(self) -> int:
        metadatum_id = len(self.metadatum_bbox2d_list)
        metadatum_bbox2d = FaceSynthetics.Metadatum.Bbox2dMetadatum(
            id_ = metadatum_id,
            class_map_id = self.METADATUM_BBOX2D_CLASSMAP_ID,
            confidence = self.METADATUM_BBOX2D_CONFIDENCE,
            creation_date = "2018-12-31T12:58:12Z",
            bbox2d_category_id = self.CATEGORY_BBOX2D_ID
        )
        self.metadatum_segmentation_list.append(metadatum_bbox2d)
        return metadatum_bbox2d.id_
    
    def create_metadatum_keypoints2d(self, class_map_id:int) -> int:
        metadatum_id = len(self.metadatum_keypoints2d_list)
        metadatum_keypoints2d = FaceSynthetics.Metadatum.Keypoints2dMetadatum(
            id_ = metadatum_id,
            class_map_id = class_map_id,
            confidence = self.METADATUM_KEYPOINTS2D_CONFIDENCE,
            creation_date = "2018-12-31T12:58:12Z",
            keypoints2d_category_id = self.CATEGORY_KEYPOINTS2D_ID
        )
        self.metadatum_keypoints2d_list.append(metadatum_keypoints2d)
        return metadatum_keypoints2d.id_
        
    def create_metadatum_segmentation(self) -> int:
        metadatum_id = len(self.metadatum_segmentation_list)
        metadatum_segmentation = FaceSynthetics.Metadatum.SegmentationMetadatum(
            id_ = metadatum_id,
            creation_date = "2018-12-31T12:58:12Z",
            confidence = self.METADATUM_SEGMENTATION_CONFIDENCE,
            segmentation_category_id = self.CATEGORY_SEGMENTATION_ID
        )
        self.metadatum_segmentation_list.append(metadatum_segmentation)
        return metadatum_segmentation.id_

    def create_annotation_metadatum(self, input_file: Path = None, image_id: int = 0, image_format: str='png', ds_image: FaceSynthetics.Source.Image = None) -> int:
    
        # Create FaceSynthetics.Annotation - warning no duplication detection
        bbox2d_annoation_list = []
        keypoints2d_annoation_list = []
        segmentation_annoation_list = []
        
        # Coco Keypoint Visibility
        COCO_KP_V_NOT_LABELLED     = 0
        COCO_KP_V_NOT_VISIBILE     = 1
        COCO_KP_V_VISIBILE         = 2
        
        keypoint_file = str(input_file).replace(f".{image_format}", "_ldmks.txt")
        seg_file = str(input_file).replace(f".{image_format}", f"_seg.{image_format}")
        
        # Create keypoints
        keypoint_index = 1 # count from 1
        min_x, min_y = ds_image.width, ds_image.height;
        max_x, max_y = 0.0, 0.0;
        
        with open(keypoint_file) as fid:
            for line in fid:
            
                # Read keypoint and set visibility information
                loc_x, loc_y = line.strip().split(sep=" ")
                x = float(loc_x)
                y = float(loc_y)
                metadatum_id = self.create_metadatum_keypoints2d(keypoint_index)               
                ds_annoation_keypoint2d = FaceSynthetics.Annotation.Keypoints2dAnnotation(
                    x,
                    y,
                    visible = COCO_KP_V_NOT_LABELLED,
                    keypoints2d_metadatum_id = metadatum_id
                )
                keypoints2d_annoation_list.append(ds_annoation_keypoint2d)
                
                # Find tight bounding box around landmarks
                if x > max_x:
                    max_x = x
                if x < min_x:
                    min_x = x
                if y > max_y:
                    max_y = y
                if y < min_y:
                    min_y = y   

                keypoint_index = keypoint_index + 1

        # Tight bounding box around landmarks
        metadatum_id = self.create_metadatum_bbox2d() 
        ds_annoation_bbox2d = FaceSynthetics.Annotation.Bbox2dAnnotation(
            top = float(min_x),
            left = float(min_y),
            width = float(max_x - min_x),
            height = float(max_y - min_y),
            bbox2d_metadatum_id = metadatum_id
        )
        bbox2d_annoation_list.append(ds_annoation_bbox2d)
        
        # Segmentation annotation file
        metadatum_id = self.create_metadatum_segmentation()
        ds_annoation_segmentation = FaceSynthetics.Annotation.SegmentationAnnotation(
            image_ref = seg_file,
            segmentation_metadatum_id = metadatum_id
        )
        segmentation_annoation_list.append(ds_annoation_segmentation)
        
        # Create annotation and append
        ds_annoation = FaceSynthetics.Annotation(
            id_ = image_id,
            image_id = image_id,
            bbox2d_annotation = bbox2d_annoation_list,
            keypoints2d_annotation = keypoints2d_annoation_list,
            segmentation_annotation = segmentation_annoation_list
        )
        self.annotation_list.append(ds_annoation)
        
        return ds_annoation.id_, ds_annoation
        
    def extract(self, ds_category:FaceSynthetics.Category=None):
    
        # Use the category loaded from the json datamodel on creation
        if ds_category == None:
            ds_category = self.category
    
        # Search for all images in folder removing segmentation maps 
        img_path = Path(self.root_dir) / self.samples_dir
        images = img_path.glob(f'*.{self.image_ext}')
        input_files = [path for path in images if '_seg' not in path.name]        
        if not input_files: raise Exception(f"No images found in {self.root_dir}")

        # Create FaceSynthetics.Source.Sensor
        sesnor_id, sensor = self.create_source_sensor()
        
        # Add each source image to dataset
        for index, file in enumerate(input_files):
            # Create FaceSynthetics.Source.Image              
            image_id, image = self.create_source_image( file.name.replace(f".{self.image_ext}", ""), file.name )
            
            # Create FaceSynthetics.Metadatum (bbox2d, keypoints2d, segmentation) and FaceSynthetics.Annotation
            annotation_id, annotation = self.create_annotation_metadatum( file, image_id, self.image_ext, image)
            
            print(f"At image index = {index}, created image_id = {image_id}, annotation_id = {annotation_id}")
        
        # Create FaceSynthetics dataset
        self.source = FaceSynthetics.Source( self.image_list, self.sensor_list)
        self.metadatum = FaceSynthetics.Metadatum( self.metadatum_bbox2d_list, self.metadatum_keypoints2d_list, self.metadatum_segmentation_list )
        self.category = FaceSynthetics.Category( self.category_bbox2d_list, self.category_keypoints2d_list, self.category_segmentation_list )
        self.dataset = FaceSynthetics( self.source, self.sample_list, self.annotation_list, self.metadatum, self.category )
        
    def visualise_segmentation(self, image:FaceSynthetics.Source.Image, annotation:FaceSynthetics.Annotation, filename:str=None):
        """Generate segmentation overlay."""
        # Read original image and segmentation
        img_path = Path(self.root_dir) / self.samples_dir / str(image.image_ref)
        image_orig: np.ndarray = imread(str(img_path))
        image_label: np.ndarray = imread(str(annotation.segmentation_annotation[self.CATEGORY_SEGMENTATION_ID].image_ref))
        
        colors = [tuple(c / 255 for c in e) for e in self.PALETTE]
        names = [e for e in self.CLASSES]
        cmap = ListedColormap(colors, 'facesynthetics')
        
        plt.imshow(image_orig, cmap="gray")
        plt.imshow(image_label, cmap=cmap, interpolation='nearest', alpha=0.5, vmin=0, vmax=20)
        
        # Create a Rectangle patch for bounding box
        bbox2d = annotation.bbox2d_annotation[self.CATEGORY_BBOX2D_ID]
        rect = patches.Rectangle((bbox2d.top, bbox2d.left), bbox2d.width, bbox2d.height, linewidth=1, edgecolor='r', facecolor='none')
        plt.gca().add_patch(rect)
        
        if filename is not None:
            plt.savefig(filename)
    
    def visualise_keypoints2d(self, keypoints2d_annotation, img_size=(512,512), filename:str=None, covariance = 3):
        """Generate heatmap for all keypoints2d."""
        # Get 68 points from annotation
        width, height = img_size
        landmarks_2d = []
        for idx,keypoint in enumerate(keypoints2d_annotation):
            if idx < 68:
                landmarks_2d.append([keypoint.x / width, keypoint.y / height])
    
        maps = []
        centers = []
        for landmark in landmarks_2d:
            centers.append([width * landmark[0], height * landmark[1]])
            
        # Obtains probability distribution heatmap (one channel) for a given set of points
        gaussians = []
        for x, y in centers:
            s = np.eye(2) * covariance
            g = multivariate_normal(mean=(x, y), cov=s)
            gaussians.append(g)
        x = np.arange(0, width)
        y = np.arange(0, height)
        xx, yy = np.meshgrid(x, y)
        xxyy = np.stack([xx.ravel(), yy.ravel()]).T
        dist = sum(g.pdf(xxyy) for g in gaussians)
        heatmap = dist.reshape((height, width))
        
        # Save heatmap
        matplotlib.image.imsave(filename, heatmap*255)
        
        return heatmap
        
if __name__ == '__main__': 
    
    # Create FaceSynthetics dataset from root folder
    dataset_100 = FaceSyntheticsDataset(
        datamodel='../facesynthetics.datamodel.json',
        root_dir='./',
        samples_dir='dataset_100',
        image_ext='png'
    )
    
    # Extract dataset datamodel from local source files
    dataset_100.extract()
    
    # Visualize sample to check correctness of JSON datamodel
    image, annotation, global_intrinsics, bbox_intrinsics = dataset_100[55]
    dataset_100.visualise_segmentation( image, annotation, "000055_vis_segmentation.png" )
    dataset_100.visualise_keypoints2d( annotation.keypoints2d_annotation, (512, 512), "000055_vis_heatmap.png" )

    #prettyprinter.install_extras(['attrs'])
    #prettyprinter.cpprint(dataset_100.dataset)
    


   
    

